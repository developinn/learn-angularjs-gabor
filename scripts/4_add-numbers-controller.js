angular.module("AddNumbersApp", [])
    .controller("AddNumbersController", function ($scope) {
        $scope.add = function () {
            $scope.sum = Number($scope.a || 0) + Number($scope.b || 0);
        };
    });