angular.module("CounterApp", [])
    .controller("CounterController", function ($scope) {
        $scope.counter = 0;
        $scope.increment = function () {
            $scope.counter++;
        }
        $scope.decrement = function () {
            $scope.counter--;
        }
    });