angular.module('HelloAngularApp', [])
    .controller('HelloAngularController', function ($scope) {
        $scope.change_name = function () {
            $scope.greeting = "Hello, " + $scope.name + " !";
        };
    });